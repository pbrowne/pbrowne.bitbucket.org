var structvar__data_1_1var__control__type =
[
    [ "cg_eps", "structvar__data_1_1var__control__type.html#a23ca1140c5a7d61b925bf035857906b8", null ],
    [ "cg_method", "structvar__data_1_1var__control__type.html#a1e7fe145075e71e9cf4c3eef323d2f7e", null ],
    [ "l", "structvar__data_1_1var__control__type.html#a4535b7553919773b595cb5a105f650ee", null ],
    [ "lbfgs_factr", "structvar__data_1_1var__control__type.html#a6e5ea644abd8f1aaac34ed86d9b3db6c", null ],
    [ "lbfgs_pgtol", "structvar__data_1_1var__control__type.html#a9457b9bf40251020fed295278d14cc15", null ],
    [ "n", "structvar__data_1_1var__control__type.html#a291d0f95bba1276e64ff5df751ebd064", null ],
    [ "nbd", "structvar__data_1_1var__control__type.html#a37bdabb380fa776fcaac6891e1e3e646", null ],
    [ "ny", "structvar__data_1_1var__control__type.html#a71c1da86e803fc0dc43bfb59aa849ce4", null ],
    [ "opt_method", "structvar__data_1_1var__control__type.html#a0fccfab01a15146ce823a385da8ab6a3", null ],
    [ "total_timesteps", "structvar__data_1_1var__control__type.html#a85a861d3f4f5aeb9f8d03b8e3756e2be", null ],
    [ "u", "structvar__data_1_1var__control__type.html#a2c53eee8c060fe97aa535f11317676ba", null ],
    [ "x0", "structvar__data_1_1var__control__type.html#af838855e75c5cfdf38ddfce13615d7fd", null ]
];