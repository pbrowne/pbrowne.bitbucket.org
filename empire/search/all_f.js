var searchData=
[
  ['other_20empire_20features',['Other EMPIRE features',['../features.html',1,'']]],
  ['objective_5ffunction',['objective_function',['../_c_g_09_2_m_p_i_2objective__function_8f90.html#a4049640baf9b2f9c95b6b93914508ac6',1,'objective_function(n, x, f):&#160;objective_function.f90'],['../_c_g_09_2objective__function_8f90.html#a4049640baf9b2f9c95b6b93914508ac6',1,'objective_function(n, x, f):&#160;objective_function.f90'],['../_lbfgsb_83_80_2objective__function_8f90.html#a4049640baf9b2f9c95b6b93914508ac6',1,'objective_function(n, x, f):&#160;objective_function.f90']]],
  ['objective_5ffunction_2ef90',['objective_function.f90',['../_c_g_09_2_m_p_i_2objective__function_8f90.html',1,'']]],
  ['objective_5ffunction_2ef90',['objective_function.f90',['../_c_g_09_2objective__function_8f90.html',1,'']]],
  ['objective_5ffunction_2ef90',['objective_function.f90',['../_lbfgsb_83_80_2objective__function_8f90.html',1,'']]],
  ['objective_5fgradient',['objective_gradient',['../_c_g_09_2_m_p_i_2objective__gradient_8f90.html#ad0770eb2caaf044adeb4dcbd7f6e1d53',1,'objective_gradient(n, x, g):&#160;objective_gradient.f90'],['../_c_g_09_2objective__gradient_8f90.html#ad0770eb2caaf044adeb4dcbd7f6e1d53',1,'objective_gradient(n, x, g):&#160;objective_gradient.f90'],['../_lbfgsb_83_80_2objective__gradient_8f90.html#ad0770eb2caaf044adeb4dcbd7f6e1d53',1,'objective_gradient(n, x, g):&#160;objective_gradient.f90']]],
  ['objective_5fgradient_2ef90',['objective_gradient.f90',['../_c_g_09_2_m_p_i_2objective__gradient_8f90.html',1,'']]],
  ['objective_5fgradient_2ef90',['objective_gradient.f90',['../_c_g_09_2objective__gradient_8f90.html',1,'']]],
  ['objective_5fgradient_2ef90',['objective_gradient.f90',['../_lbfgsb_83_80_2objective__gradient_8f90.html',1,'']]],
  ['obs_5fdim',['obs_dim',['../classsizes.html#a5f5dd89a39d1254b4ac7f7daf2280518',1,'sizes']]],
  ['obs_5fdim_5fg',['obs_dim_g',['../classsizes.html#ad88227cd89a502f5a04620486dd10b71',1,'sizes']]],
  ['obs_5fdims',['obs_dims',['../classcomms.html#ac13d6d340b5ba87d70a203aec1e34866',1,'comms']]],
  ['obs_5fdisplacements',['obs_displacements',['../classcomms.html#a54caf65143568611633cd42cdfe90e00',1,'comms']]],
  ['obs_5ftimes',['obs_times',['../structtimestep__data_1_1timestep__data__type.html#a1dcbd8ffdb446dab619c4700c9d68809',1,'timestep_data::timestep_data_type']]],
  ['open_5femp_5fo',['open_emp_o',['../classoutput__empire.html#a2513fbde22a5469806a6a1ca1b92f7e9',1,'output_empire']]],
  ['operator_5fwrappers_2ef90',['operator_wrappers.f90',['../operator__wrappers_8f90.html',1,'']]],
  ['opt_5fmethod',['opt_method',['../structvar__data_1_1var__control__type.html#a0fccfab01a15146ce823a385da8ab6a3',1,'var_data::var_control_type']]],
  ['opt_5fpetsc',['opt_petsc',['../classcompile__options.html#ac3f9a986b34165ce7aad77fcf8db141f',1,'compile_options']]],
  ['other_5ffeatures_2etxt',['other_features.txt',['../other__features_8txt.html',1,'']]],
  ['output_5fempire',['output_empire',['../classoutput__empire.html',1,'']]],
  ['output_5fempire_2ef90',['output_empire.f90',['../output__empire_8f90.html',1,'']]],
  ['output_5fens_5frmse',['output_ens_rmse',['../output__ens__rmse_8f90.html#a69e6216d97b0a64b921a32b1ea08ce30',1,'output_ens_rmse.f90']]],
  ['output_5fens_5frmse_2ef90',['output_ens_rmse.f90',['../output__ens__rmse_8f90.html',1,'']]],
  ['output_5fforecast',['output_forecast',['../structpf__control_1_1pf__control__type.html#a1e3d8842f67a492a50f49d30e6dd64b4',1,'pf_control::pf_control_type::output_forecast()'],['../output__forecast_8f90.html#ae3ecdfc776fe49dd00f8fe19c957c244',1,'output_forecast():&#160;output_forecast.f90']]],
  ['output_5fforecast_2ef90',['output_forecast.f90',['../output__forecast_8f90.html',1,'']]],
  ['output_5ffrom_5fpf',['output_from_pf',['../data__io_8f90.html#a4b7f0fc2733276e4b8b074f089c7b4fb',1,'data_io.f90']]],
  ['output_5fmat_5ftri',['output_mat_tri',['../output__mat__tri_8f90.html#ad261946f22402e1bf4135ad7305fd072',1,'output_mat_tri.f90']]],
  ['output_5fmat_5ftri_2ef90',['output_mat_tri.f90',['../output__mat__tri_8f90.html',1,'']]],
  ['output_5fspatial_5frmse',['output_spatial_rmse',['../output__spatial__rmse_8f90.html#afbe1772d452f681dff082ce848ea7c3d',1,'output_spatial_rmse.f90']]],
  ['output_5fspatial_5frmse_2ef90',['output_spatial_rmse.f90',['../output__spatial__rmse_8f90.html',1,'']]],
  ['output_5ftype',['output_type',['../structmatrix__pf_1_1matrix__pf__data.html#a991fbb66a23a617d5a31735b0498b4c1',1,'matrix_pf::matrix_pf_data']]],
  ['output_5fvariance',['output_variance',['../output__variance_8f90.html#afad69c28baa64cc5221d0c834a8171c2',1,'output_variance.f90']]],
  ['output_5fvariance_2ef90',['output_variance.f90',['../output__variance_8f90.html',1,'']]],
  ['output_5fweights',['output_weights',['../structpf__control_1_1pf__control__type.html#ab552227b41550aa5d494a82a1c6a4995',1,'pf_control::pf_control_type']]]
];
