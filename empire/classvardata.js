var classvardata =
[
    [ "var_control_type", "structvardata_1_1var__control__type.html", "structvardata_1_1var__control__type" ],
    [ "allocate_vardata", "classvardata.html#afe73f0cc697c673c1e52df55ffcf0caa", null ],
    [ "deallocate_vardata", "classvardata.html#a480ef3910e6c757051d13f9c7434d2e6", null ],
    [ "parse_vardata", "classvardata.html#a23a5cec5f2b7073409d8509ec99b8851", null ],
    [ "set_var_controls", "classvardata.html#acb8220f0e05357dbd03d7e3cfc8f6c3b", null ],
    [ "vardata", "classvardata.html#aa73e3563db02af948e4c24be9eedbd4d", null ]
];