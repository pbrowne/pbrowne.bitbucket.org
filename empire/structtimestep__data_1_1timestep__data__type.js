var structtimestep__data_1_1timestep__data__type =
[
    [ "completed_timesteps", "structtimestep__data_1_1timestep__data__type.html#ac963c62ca21df7a8455415992483952f", null ],
    [ "current_timestep", "structtimestep__data_1_1timestep__data__type.html#a5f384ffc1b87897adeb45c4df7954416", null ],
    [ "do_analysis", "structtimestep__data_1_1timestep__data__type.html#aef84db634c3f882ec9ee487a22696499", null ],
    [ "is_analysis", "structtimestep__data_1_1timestep__data__type.html#aaa46f5d956f09d1c7aa9ea61a5655dde", null ],
    [ "next_ob_timestep", "structtimestep__data_1_1timestep__data__type.html#af4d907225b0fd3edc2a2258051d6eca7", null ],
    [ "obs_times", "structtimestep__data_1_1timestep__data__type.html#a1dcbd8ffdb446dab619c4700c9d68809", null ],
    [ "tau", "structtimestep__data_1_1timestep__data__type.html#a8039e71eb44d81c31b92838f7653c0ef", null ],
    [ "total_timesteps", "structtimestep__data_1_1timestep__data__type.html#a8552a8163ba7da228ac13677395e5350", null ]
];