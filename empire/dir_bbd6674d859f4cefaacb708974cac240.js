var dir_bbd6674d859f4cefaacb708974cac240 =
[
    [ "compile_options.f90", "compile__options_8f90.html", [
      [ "compile_options", "classcompile__options.html", "classcompile__options" ]
    ] ],
    [ "empire.nml", "empire_8nml.html", null ],
    [ "empire_main.f90", "empire__main_8f90.html", "empire__main_8f90" ],
    [ "letks_test.f90", "letks__test_8f90.html", "letks__test_8f90" ],
    [ "output_empire.f90", "output__empire_8f90.html", [
      [ "output_empire", "classoutput__empire.html", "classoutput__empire" ]
    ] ],
    [ "pf_control.f90", "pf__control_8f90.html", [
      [ "pf_control", "classpf__control.html", "classpf__control" ],
      [ "pf_control_type", "structpf__control_1_1pf__control__type.html", "structpf__control_1_1pf__control__type" ]
    ] ],
    [ "sizes.f90", "sizes_8f90.html", [
      [ "sizes", "classsizes.html", "classsizes" ]
    ] ],
    [ "timestep_data.f90", "timestep__data_8f90.html", [
      [ "timestep_data", "classtimestep__data.html", "classtimestep__data" ],
      [ "timestep_data_type", "structtimestep__data_1_1timestep__data__type.html", "structtimestep__data_1_1timestep__data__type" ]
    ] ]
];