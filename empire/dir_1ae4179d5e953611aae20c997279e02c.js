var dir_1ae4179d5e953611aae20c997279e02c =
[
    [ "linear", "dir_2ed0a32817cd0221ec45fd5bf31841da.html", "dir_2ed0a32817cd0221ec45fd5bf31841da" ],
    [ "lorenz63", "dir_7616213a845630f67e9c235053ae2d5b.html", "dir_7616213a845630f67e9c235053ae2d5b" ],
    [ "lorenz96", "dir_8a8cd9aa3beeaf279ba9fec3f5c9236e.html", "dir_8a8cd9aa3beeaf279ba9fec3f5c9236e" ],
    [ "minimal_empire", "dir_7685b20c560fa533c4752f0b8133b8ed.html", "dir_7685b20c560fa533c4752f0b8133b8ed" ],
    [ "minimal_empire_comms", "dir_3315a0233599b3d9fca922b413bd94ed.html", "dir_3315a0233599b3d9fca922b413bd94ed" ],
    [ "minimal_model", "dir_0a71f4dcbc2d99a533fd7dbb8c506da1.html", "dir_0a71f4dcbc2d99a533fd7dbb8c506da1" ],
    [ "minimal_model_comms", "dir_20fb7ac6dc1f6c2e9372ebb278bb97ff.html", "dir_20fb7ac6dc1f6c2e9372ebb278bb97ff" ]
];