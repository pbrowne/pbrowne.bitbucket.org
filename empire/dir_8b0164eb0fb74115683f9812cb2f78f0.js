var dir_8b0164eb0fb74115683f9812cb2f78f0 =
[
    [ "model", "dir_6ea1753c3b442ff31481cde1ecb796e5.html", "dir_6ea1753c3b442ff31481cde1ecb796e5" ],
    [ "Bdata.f90", "_bdata_8f90.html", [
      [ "bdata", "classbdata.html", "classbdata" ]
    ] ],
    [ "Qdata.f90", "_qdata_8f90.html", [
      [ "qdata", "classqdata.html", "classqdata" ]
    ] ],
    [ "Rdata.f90", "_rdata_8f90.html", [
      [ "rdata", "classrdata.html", "classrdata" ],
      [ "hqht_plus_r", "classhqht__plus__r.html", "classhqht__plus__r" ]
    ] ],
    [ "user_initialise_mpi.f90", "user__initialise__mpi_8f90.html", "user__initialise__mpi_8f90" ],
    [ "user_perturb_particle.f90", "user__perturb__particle_8f90.html", "user__perturb__particle_8f90" ],
    [ "user_relaxation_profile.f90", "user__relaxation__profile_8f90.html", "user__relaxation__profile_8f90" ]
];