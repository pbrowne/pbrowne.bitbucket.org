var tutorials =
[
    [ "Lorenz 96 Tutorial", "tutorial_lorenz96.html", [
      [ "Description of the model", "tutorial_lorenz96.html#tl", null ],
      [ "Connecting the model to EMPIRE using MPI.", "tutorial_lorenz96.html#tl962", null ],
      [ "Specification of subroutines in model_specific.f90", "tutorial_lorenz96.html#tutorial_l96_model_specific", [
        [ "The observations", "tutorial_lorenz96.html#tl963", [
          [ "Defining observation operators:", "tutorial_lorenz96.html#tl963a", null ],
          [ "Defining observation error covariances:", "tutorial_lorenz96.html#tl963b", null ]
        ] ],
        [ "Defining background error covariance matrix:", "tutorial_lorenz96.html#tl964", null ],
        [ "Defining model error covariance matrix:", "tutorial_lorenz96.html#tl965", null ],
        [ "Specifying distance for localisation:", "tutorial_lorenz96.html#tl966", null ],
        [ "Setting up configure model and reconfigure model for an experiment:", "tutorial_lorenz96.html#tl967", null ],
        [ "Compiling the model specific changes", "tutorial_lorenz96.html#tutorial_lorenz96_compiling", null ]
      ] ],
      [ "Running the codes", "tutorial_lorenz96.html#tutorial_lorenz96_running", [
        [ "Running the truth", "tutorial_lorenz96.html#tl968", null ],
        [ "Running a stochastic ensemble", "tutorial_lorenz96.html#tl969", null ],
        [ "Running an assimilation", "tutorial_lorenz96.html#tl9610", null ]
      ] ],
      [ "Plotting the results", "tutorial_lorenz96.html#tl9611", null ],
      [ "Tutorial codes", "tutorial_lorenz96.html#tutorial_lorenz96_codes", null ]
    ] ],
    [ "Storing operators and data in modules - a tutorial", "tutorial_modules.html", [
      [ "Motivation", "tutorial_modules.html#mtmotivation", null ],
      [ "Location of template modules", "tutorial_modules.html#mtdatafiles", null ],
      [ "Description of an example B matrix", "tutorial_modules.html#mtbdescription", null ],
      [ "Description of the module", "tutorial_modules.html#mtbdatadescription", null ],
      [ "Description of the loading subroutine", "tutorial_modules.html#mtloadbdescription", null ],
      [ "Calling the loadB subroutine", "tutorial_modules.html#mtcalling", null ],
      [ "Using the data that has been read in", "tutorial_modules.html#mtuse", null ]
    ] ],
    [ "Using PETSc for the model specific operators - a tutorial", "tutorial_petsc.html", null ]
];